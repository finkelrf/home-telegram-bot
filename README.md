# Home Telegram Bot

Personal telegram bot to connect with some of the services that I run at my home

## Install
```
# Use -e (editable) for development
pip install .
```

## Run
Update .env file with the correct API keys and URLs then run:
```
set -a
source .env
set +a
telegram-bot
```

## Run tests
After install the package with pip run:
```
pytest
```

## Run Telegram bot
After install the package with pip run:
```
telegram-bot
```