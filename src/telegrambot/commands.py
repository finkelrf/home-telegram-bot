import subprocess

from telegrambot.utils import Ping
from .db import Measurements
from .db import Rooms
from .alerts import Alert, AlertType, PingAlertHandler
from .alerts import AlertsManager


class Command:
    def __init__(self) -> None:
        self.return_msg = None


class CommandParseError(Exception):
    pass


class PingAlertCommand(Command):
    def __init__(self, ping_alert_register_manager, args, chat_id):
        self._register_manager = ping_alert_register_manager
        self._command_type, self._ip = args
        self._chat_id = chat_id

    def run(self):
        try:
            if self._command_type == "register":
                self._register_ip_alert()
            elif self._command_type == "remove":
                self._remove_ip_alert()
            return f"Ping alerts: {self._register_manager.list_ping_alerts(self._chat_id)}"
        except KeyError:
            return "Operation failed"

    def _register_ip_alert(self):
        self._register_manager.register(self._chat_id, self._ip)
        return f"IP {self._ip} registered"

    def _remove_ip_alert(self):
        self._register_manager.remove(self._chat_id, self._ip)
        return f"IP {self._ip} removed"

class PingCommand(Command):
    def __init__(self, ip):
        super().__init__()
        self._ip = ip

    def run(self):
        return f"{self._ip} is {'connected' if Ping().is_ip_connected(self._ip) else 'not connected'}"


class TemperatureCommand(Command):
    def __init__(self, db) -> None:
        super().__init__()
        self.db = db

    def run(self):
        msg = ''
        for room in Rooms:
            data = self.db.query_room(room, Measurements.temperature, True)
            msg += f"{room.name.replace('_', ' ').capitalize()}: "
            if len(data) > 0:
                msg += f"{data[0].value}°C"
            else:
                msg += 'No data'
            msg += '\n'
        return msg


class BatteryCommand(Command):
    def __init__(self, db) -> None:
        super().__init__()
        self.db = db

    def run(self):
        msg = ''
        for room in Rooms:
            data = self.db.query_room(room, Measurements.battery_mv, True)
            msg += f"{room.name.replace('_', ' ').capitalize()}: "
            if len(data) > 0:
                msg += f"{data[0].value}mv"
            else:
                msg += 'No data'
            msg += '\n'

        return msg


class AlertCommand(Command):
    def __init__(self, chat_id, alerts_manager: AlertsManager) -> None:
        super().__init__()
        self.alerts_manager: AlertsManager = alerts_manager
        self.chat_id = chat_id

    def get_alert_help_msg(self):
        return (f"Register one with /alert <mqtt_topic> "
                f"<{'/'.join([ str(e) for e in list(AlertType) ])}> "
                f"<value_threshold>")

    def get_registered_alerts(self, chat_id, human_readable=True):
        alerts = self.alerts_manager.alerts.values()
        user_alerts = [
            alert for alert in alerts if alert.chat_id == int(chat_id)]
        if not human_readable:
            return user_alerts
        else:
            msg = '\n'.join(
                [str(alert) for alert in alerts])
            if not msg:
                msg = "No alerts registered. "+self.get_alert_help_msg()
            return msg

    def register_alert(self, args):
        topic, alert_type, threshold = args
        alert = Alert(self.chat_id, topic=topic,
                      alert_type=alert_type, threshold=threshold)
        self.alerts_manager.register(alert)

    def run(self, args):
        if len(args) == 0:
            return self.get_registered_alerts(self.chat_id)
        else:
            if args[0] == "help":
                return self.get_alert_help_msg()
            else:
                self.register_alert(args)
                return "Alert registered"
