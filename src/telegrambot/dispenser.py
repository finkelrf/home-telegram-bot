from abc import ABC, abstractmethod
from enum import Enum
import random
import threading
import time

from .commands import Command, CommandParseError
from .mqtt import Mqtt


class DispenserService(ABC):
    @abstractmethod
    def send_dispense(self):
        pass


class MqttDispenserService(DispenserService):
    TOPIC = '/treat_dispenser/dispense'
    DISPENSE_MESSAGE = 'telegrambot'

    def __init__(self, mqtt: Mqtt):
        self._mqtt = mqtt

    def send_dispense(self):
        self._mqtt.publish(self.TOPIC, self.DISPENSE_MESSAGE)


class DispenserManager(Command):
    def __init__(self, dispenser_service: DispenserService):
        super().__init__()
        self._dispenser_service = dispenser_service
        self._duration = None
        self._repetitions = None
        self._is_stopped = True
        self._next_command = None
        self._response = []

    @property
    def response(self):
        return self._response.pop(0)

    def parse_message(self, message: str):
        try:
            parser = DispenserCommandParser(message)
            parser.parse()
            self._fill_in_parameters(parser.command, parser.parameters)
        except (ValueError, CommandParseError):
            raise CommandParseError(
                'Wrong command format. Command example "/treat <time in minutes> <number of dispenses>')

    def do_command(self):
        if not self._next_command:
            raise Exception("No command to be executed")
        if self._next_command == DispenserCommands.START:
            print('Do command: start')
            self._start()
        elif self._next_command == DispenserCommands.STOP:
            print('Do command: stop')
            self._stop()

    def _start(self):
        self._thread = threading.Thread(target=self._dispense_task)
        self._is_stopped = False
        self._thread.start()
        self._response.append(
            f"Dispensing started. Times: {', '.join([str(int(interval)) for interval in self._intervals])}")

    def _stop(self):
        self._is_stopped = True
        self._response.append("Dispensing stopped")

    def _fill_in_parameters(self, command, parameters):
        self._next_command = command
        self._duration = parameters['duration'] * \
            60 if 'duration' in parameters else None
        self._repetitions = parameters['repetitions'] if 'repetitions' in parameters else None
        self._generate_intervals()

    def _generate_intervals(self):
        if self._duration and self._repetitions:
            self._intervals = sorted([
                random.random()*self._duration for _ in range(self._repetitions)])
            print(self._intervals)

    def _dispense(self):
        print('Dispensing treats!')
        self._dispenser_service.send_dispense()

    def _dispense_task(self):
        time_elapsed = 0
        while (not self._is_stopped and self._intervals):
            interval = self._intervals.pop(0)
            time.sleep(interval-time_elapsed)
            time_elapsed = interval
            if not self._is_stopped:
                self._dispense()
        print('Dispense task stopped')


class DispenserCommandParser:
    def __init__(self, command_message):
        self._message = command_message
        self._command = None
        self._parameters = {}

    @property
    def command(self):
        return self._command

    @property
    def parameters(self):
        return self._parameters

    def parse(self):
        words = self._message.split()
        if len(words) == 2 and words[1] == 'stop':
            self._command = DispenserCommands.STOP
            print('Got Stop command')
        elif len(words) == 3:
            self._command = DispenserCommands.START
            self._parameters['duration'] = int(words[1])
            self._parameters['repetitions'] = int(words[2])
            print('Got Start command')
        else:
            raise CommandParseError()


class DispenserCommands(Enum):
    START = 'start'
    STOP = 'stop'
