import paho.mqtt.client as mqtt
import threading
import time

class MqttNotConnectedException(Exception):
    pass

class MqttConfig():
    MQTT_HOST = "localhost"
    MQTT_PORT = 1883


class Mqtt():
    def __init__(self, config, on_msg_callback):
        self.client = mqtt.Client()
        self.client.on_connect = self.on_connect
        self.client.on_message = self.on_message
        self.on_msg_callback = on_msg_callback

        self.client.connect(config.MQTT_HOST, config.MQTT_PORT, 60)
        #self.client.username_pw_set(config.MQTT_USER, config.MQTT_PASSWORD)

    def loop(self):
        self.client.loop_forever()

    # The callback for when the client receives a CONNACK response from the server.
    def on_connect(self, client, userdata, flags, rc):
        print("Connected!")

    # The callback for when a PUBLISH message is received from the server.
    def on_message(self, client, userdata, msg):
        print(msg.topic+" "+str(msg.payload))
        self.on_msg_callback(client, userdata, msg)

    def _subscribe_async(self, topic):
        while not self.client.is_connected():
            print("MQTT not connected, waiting 100 ms")
            time.sleep(0.1)
        self.client.subscribe(topic)

    def subscribe_async(self, topic):
        th = threading.Thread(target=self._subscribe_async, args=(topic,))
        th.start()

    def publish(self, topic, message):
        self.client.publish(topic, message)

class MqttUtils():
    @staticmethod
    def compare_topics(topic1: str, topic2: str) -> bool:
        topic2_parts = topic2.split('/')
        topic1_parts = topic1.split('/')
        for part1, part2 in zip(topic1_parts, topic2_parts):
            if '#' in part1 or '#' in part2:
                return True
            if part1 != part2:
                return False
        return True