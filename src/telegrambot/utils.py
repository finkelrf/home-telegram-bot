
import subprocess


class Ping:
    def is_ip_connected(self, ip):
        try:
            # Run the ping command with a timeout
            result = subprocess.run(
                ["ping", "-c", "1", "-W", "2", ip],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE
            )
            # Check if the ping was successful
            return result.returncode == 0
        except Exception as e:
            print(f"An error occurred: {e}")
