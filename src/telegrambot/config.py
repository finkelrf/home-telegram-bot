import os

from .mqtt import MqttConfig as MqttConfigBase
from .baseconfig import InfluxDbConfigBase

LOGGING_FILEPATH = os.getenv("LOGGING_FILEPATH", 'telebot.log')

class BotConfig():
    BOT_TOKEN = os.getenv("BOT_TOKEN", None)
    ALERT_FILE = os.getenv("ALERT_FILE", "alerts.json")
    PING_ALERTS_FILE = os.getenv("PING_ALERTS_FILE", "ping_alerts.json")

class InfluxDbConfig(InfluxDbConfigBase):
    token = os.getenv("INFLUXDB_TOKEN", None)
    org = "home"
    url = F"http://{os.getenv('INFLUXDB_URL', 'localhost')}:{os.getenv('INFLUXDB_PORT', '8086')}"
    measurement = "telemetry"
    bucket = "home"

class MqttConfig(MqttConfigBase):
    MQTT_HOST = os.getenv('MQTT_HOST', 'localhost')