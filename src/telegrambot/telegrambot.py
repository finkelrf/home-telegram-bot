import telebot
import time
from enum import Enum
from paho.mqtt.client import MQTTMessage
from telebot.types import Message as TelebotMsg


from .alerts import AlertType, PingAlertHandler, PingAlertRegisterManager
from .alerts import AlertsManager
from .commands import AlertCommand, CommandParseError, PingAlertCommand, PingCommand
from .commands import BatteryCommand
from .commands import TemperatureCommand
from .dispenser import DispenserManager, MqttDispenserService
from .mqtt import Mqtt
from .mqtt import MqttUtils


class Commands(Enum):
    TREAT = ['treat']
    START = ['start', 'hello']
    BATTERY = ['battery', 'bat']
    TEMPERATURE = ['temperature', 'temp']
    ALERT = ['alert']
    PING = ['ping']
    PING_ALERT = ['ping_alert', 'ping-alert']


class TelegramBot():
    def __init__(self, token, db, mqtt: Mqtt, load_alerts=False) -> None:
        self._bot = telebot.TeleBot(token)
        self._db = db
        self._mqtt = mqtt
        self._mqtt.on_msg_callback = self.alerts_handler
        self.alerts_manager = AlertsManager(mqtt)
        dispenser_service = MqttDispenserService(self._mqtt)
        self.dispenser_manager = DispenserManager(dispenser_service)
        if load_alerts:
            self.alerts_manager.load_alerts()
        self.ping_alert_register_manger = PingAlertRegisterManager()
        self.ping_alert_handler = PingAlertHandler(
            self._bot, self.ping_alert_register_manger)
        self.ping_alert_handler.start()

        @self._bot.edited_message_handler(commands=Commands.PING_ALERT.value)
        @self._bot.message_handler(commands=Commands.PING_ALERT.value)
        def ping_alert_handler(message: TelebotMsg):
            self.run_ping_alert_handler(message.chat.id, message.text)


        @self._bot.edited_message_handler(commands=Commands.PING.value)
        @self._bot.message_handler(commands=Commands.PING.value)
        def ping_device(message: TelebotMsg):
            try:
                ip = message.text.split()[1]
                msg = PingCommand(ip).run()
                self._bot.send_message(message.chat.id, msg)
            except IndexError:
                error_msg = "Wrong command format. Try /ping 192.168.1.XXX"
                self._bot.send_message(message.chat.id, error_msg)

        @self._bot.edited_message_handler(commands=Commands.TREAT.value)
        @self._bot.message_handler(commands=Commands.TREAT.value)
        def dispense_treat(message: TelebotMsg):
            try:
                self.dispenser_manager.parse_message(message.text)
                self.dispenser_manager.do_command()
                self._reply_message(
                    message.chat.id, self.dispenser_manager.response)
            except (CommandParseError, IndexError) as e:
                self._bot.send_message(message.chat.id, e)

        @self._bot.edited_message_handler(commands=Commands.START.value)
        @self._bot.message_handler(commands=Commands.START.value)
        def send_welcome(message: TelebotMsg):
            self._bot.send_message(message.chat.id, "Banana")

        @self._bot.edited_message_handler(commands=Commands.BATTERY.value)
        @self._bot.message_handler(commands=Commands.BATTERY.value)
        def send_battery(message: TelebotMsg):
            msg = BatteryCommand(self._db).run()
            self._bot.send_message(message.chat.id, msg)

        @self._bot.edited_message_handler(commands=Commands.TEMPERATURE.value)
        @self._bot.message_handler(commands=Commands.TEMPERATURE.value)
        def send_temperature(message: TelebotMsg):
            msg = TemperatureCommand(self._db).run()
            self._bot.send_message(message.chat.id, msg)

        @self._bot.edited_message_handler(commands=Commands.ALERT.value)
        @self._bot.message_handler(commands=Commands.ALERT.value)
        def register_alert(message: TelebotMsg):
            ac = AlertCommand(message.chat.id, self.alerts_manager)
            response = ac.run(message.text.split()[1:])
            self._bot.send_message(message.chat.id, response)

        @self._bot.edited_message_handler(func=lambda message: True)
        @self._bot.message_handler(func=lambda message: True)
        def default_message_handler(message: TelebotMsg):
            response = "Available commands: "
            response += " - ".join(["/"+c.value[0] for c in Commands])
            self._bot.send_message(message.chat.id, response)

    def run_ping_alert_handler(self, chat_id, message_text):
        msg = 'a'
        try:
            msg = PingAlertCommand(
                self.ping_alert_register_manger, message_text.split()[1:], chat_id).run()
        except ValueError:
            msg = "Fail to parse command. Try:\n /ping_alert register 192.168.1.XXX\n/ping_alert remove 192.168.1.XXX"
        self._bot.send_message(chat_id, msg)

    def alerts_handler(self, client, userdata, message: MQTTMessage):
        for alert in self.alerts_manager.alerts.values():
            alert_triggered = False
            if MqttUtils.compare_topics(alert.topic, message.topic):
                if (alert.alert_type == AlertType.HIGHER):
                    if (float(message.payload) > alert.threshold):
                        alert_triggered = True
                elif (alert.alert_type == AlertType.LOWER):
                    if (float(message.payload) < alert.threshold):
                        alert_triggered = True
                elif alert.alert_type == AlertType.ANY:
                    alert_triggered = True

                if (alert_triggered):
                    print("Alert triggered")
                    alert.triggered = True
                    self._bot.send_message(alert.chat_id,
                                           f"ALERT! {message.topic} is {alert.alert_type.name} then {alert.threshold}\n" +
                                           f"Current value: {message.payload}")
                else:
                    print("Alert NOT triggered")

    def loop(self):
        print(f'Starting Telebot infinite loop')
        try:
            self._bot.infinity_polling()
            raise Exception('testing exception')
        except Exception as e:
            print(e)
            time.sleep(10)

    def _reply_message(self, chat_id, message):
        max_response_length = 4096
        sub_messages = [message[i:i+max_response_length]
                        for i in range(0, len(message), max_response_length)]
        for sub_message in sub_messages:
            self._bot.send_message(chat_id, sub_message)
