#!/usr/bin/python3
import logging
import threading

from .db import InfluxDb
from .telegrambot import TelegramBot
from .config import BotConfig
from .config import InfluxDbConfig
from .config import MqttConfig
from .config import LOGGING_FILEPATH
from .mqtt import Mqtt

def main():
    logging.basicConfig(filename=LOGGING_FILEPATH,
                        filemode='w',
                        format='%(name)s - %(levelname)s - %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging.DEBUG)

    mqtt = Mqtt(MqttConfig, None)
    db = InfluxDb(InfluxDbConfig)
    bot = TelegramBot(BotConfig.BOT_TOKEN, db, mqtt, load_alerts=True)

    mqtt_loop_thread = threading.Thread(target=mqtt.loop)
    bot_loop_thread = threading.Thread(target=bot.loop)

    mqtt_loop_thread.start()
    bot_loop_thread.start()
