import influxdb_client
import os
import time
from influxdb_client import InfluxDBClient, Point, WritePrecision
from influxdb_client.client.write_api import SYNCHRONOUS
from influxdb_client.client.flux_table import FluxTable
from typing import TypedDict
from datetime import datetime
from enum import Enum


class Rooms(Enum):
    office = 0
    living_room = 1
    outside = 2
    bedroom = 3
    storage = 4


class Measurements(Enum):
    temperature = 0,
    heat_index = 1,
    humidity = 2,
    battery_mv = 3,


class SimpleData():

    def __init__(self) -> None:
        self.time: datetime = None
        self.value: float = None
        self.field: str = None

    def __str__(self) -> str:
        return f"""
            time: {self.time}
            value: {self.value}
            field: {self.field}
            """


class InfluxRange(TypedDict):
    start: str
    stop: str


class InfluxDb():
    def __init__(self, config):
        self._config = config
        self._measurement = config.measurement
        self._point = Point(self._measurement)
        self._client = influxdb_client.InfluxDBClient(
            url=config.url, token=config.token, org=config.org)
        self._write_api = self._client.write_api(write_options=SYNCHRONOUS)

    def add_field(self, field, value):
        self._point.field(field, value)

    def add_field_dict(self, dict):
        for key in dict:
            self._point.field(key, dict[key])

    def write(self):
        self._write_api.write(bucket=self._config.bucket,
                              org=self._config.org, record=self._point)
        self._point = Point(self._measurement)

    def clear_point(self):
        self._point = Point(self._measurement)

    def query(self, field: str, range: InfluxRange, last=False) -> FluxTable:
        query_api = self._client.query_api()

        query = F"""from(bucket: "{self._config.bucket}")
    |> range(start: {range["start"]})
    |> filter(fn: (r) => r._measurement == "telemetry" and r._field == "{field}" ) """
        if last:
            query += "\n    |> last()"

        # print(query)
        tables = query_api.query(query, org=self._client.org)

        # for table in tables:
        #     for record in table.records:
        #         print(record)

        return tables

    def query_room(self, room: Rooms, measurement: Measurements, last: bool) -> SimpleData:
        range = {"start": "-1h"}
        tables = self.query(f"/{measurement.name}/{room.name}", range, last)
        data_list = []
        for table in tables:
            for record in table.records:
                data = SimpleData()
                data.field = record["_field"]
                data.time = record["_time"]
                data.value = record["_value"]
                data_list.append(data)
        return data_list


if __name__ == "__main__":
    from config import InfluxDbConfig
    db = InfluxDb(InfluxDbConfig)
    # data = db.query("/temperature/office", {"start": "-10m"}, last=True)
    data = db.query_room(Rooms.office, Measurements.temperature, True)
    for item in data:
        print(item)
