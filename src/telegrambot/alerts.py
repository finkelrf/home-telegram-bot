import json
import logging
import os
import threading
import time
import telebot
from ast import Dict
from enum import Enum
from json import JSONEncoder

from telegrambot.utils import Ping

from . import config as config
from .mqtt import Mqtt


class Alert():
    def __init__(self,
                 chat_id: int = 0,
                 topic: str = '',
                 alert_type='higher',
                 threshold: float = 0.0,
                 interval: int = 0  # in seconds
                 ):
        alert_type = str(alert_type)
        self._chat_id = int(chat_id)
        self._topic = topic.decode() if type(topic) == bytes else topic
        self._alert_type = self.set_alert_type(alert_type)
        self._threshold = float(threshold)
        self._triggered = False
        self._interval = interval

    @property
    def chat_id(self):
        return self._chat_id

    @property
    def topic(self):
        return self._topic

    @property
    def threshold(self):
        return self._threshold

    @property
    def alert_type(self):
        return self._alert_type

    @property
    def interval(self):
        return self._interval

    @property
    def triggered(self):
        return self._triggered

    @triggered.setter
    def triggered(self, value):
        self._triggered = value

    def __str__(self) -> str:
        return (f'{self.topic} - {self.alert_type} '
                f'then {self.threshold} - '
                f'Trigg:{self.triggered} - '
                f'Interval:{self.interval}')

    def from_dict(self, dictionary):
        for k, v in dictionary.items():
            private_attr = '_'+k
            if k == 'alert_type':
                setattr(self, private_attr, self.set_alert_type(v))
            else:
                setattr(self, private_attr, v)

    def set_alert_type(self, alert_type):
        if type(alert_type) == str:
            return AlertType[alert_type.upper()]
        elif type(alert_type) == AlertType:
            return alert_type


class AlertType(str, Enum):
    HIGHER: str = "higher",
    LOWER: str = "lower",
    ANY: str = "any"

    def __str__(self) -> str:
        return self.value


class AlertEncoder(JSONEncoder):
    def default(self, o):
        return o.__dict__


class AlertsManager:
    def __init__(self, mqtt: Mqtt) -> None:
        self.mqtt = mqtt
        self.alerts: Dict[str, AlertType] = {}

    def load_alerts(self):
        self.alerts = AlertsUtils.load_alerts_from_file()
        # subscribe to alert topics
        for alert in self.alerts.values():
            self.mqtt.subscribe_async(alert.topic)

    def register(self, alert: Alert) -> None:
        self.alerts[(alert.chat_id, alert.topic, alert.alert_type)] = alert
        # subscribe to new topic
        self.mqtt.subscribe_async(alert.topic)
        AlertsUtils.write_alerts_to_file(self.alerts.values())


class AlertsUtils:
    @staticmethod
    def load_alerts_from_file():
        json_string = ''
        alerts = {}
        try:
            with open(config.BotConfig.ALERT_FILE, 'r') as file:
                json_string = file.read()
            if json_string:
                for alert in json.loads(json_string):
                    a = Alert()
                    a.from_dict(alert)
                    alerts[(a.chat_id, a.topic, a.alert_type)] = a
        except FileNotFoundError:
            logging.debug(
                f"Alert file {config.BotConfig.ALERT_FILE} not found, creating an empty alert file.")
            with open(config.BotConfig.ALERT_FILE, 'w+') as file:
                json.dump(alerts, file)
        return alerts

    @staticmethod
    def write_alerts_to_file(alerts):
        json_string = json.dumps(list(alerts), cls=AlertEncoder)
        with open(config.BotConfig.ALERT_FILE, 'w') as file:
            file.write(json_string)


class PingAlertRegisterManager:
    @property
    def alerts(self):
        return self._alerts

    def __init__(self, file_path=None):
        if file_path is None:
            file_path = config.BotConfig.PING_ALERTS_FILE
        self._file_path = file_path
        self._alerts = self._load_alerts()

    def register(self, chat_id, ip):
        self._alerts.add((chat_id, ip))
        self._save_alerts_to_file()

    def remove(self, chat_id, ip):
        self._alerts.remove((chat_id, ip))
        self._save_alerts_to_file()

    def list_ping_alerts(self, current_chat_id):
        ips = []
        for chat_id, ip in self._alerts:
            if current_chat_id == chat_id:
                ips.append(ip)
        return ", ".join(ips)

    def _save_alerts_to_file(self):
        with open(self._file_path, 'w') as file:
            file.write(json.dumps(list(self._alerts)))

    def _load_alerts(self):
        self._create_file_if_not_exists()
        self._load_alerts_file()
        return self._alerts

    def _create_file_if_not_exists(self):
        if not os.path.isfile(self._file_path):
            with open(self._file_path, 'w+') as file:
                file.write(json.dumps(list(set())))

    def _load_alerts_file(self):
        with open(self._file_path, 'r') as file:
            json_string = file.read()
            if json_string:
                self._alerts = {tuple(item)
                                for item in json.loads(json_string)}


class PingAlertHandler:
    INTERVAL = 60

    def __init__(self, bot: telebot.TeleBot, ping_alert_register_manager: PingAlertRegisterManager):
        self._bot = bot
        self._register_manager = ping_alert_register_manager
        self._stop_event = threading.Event()
        self._thread = threading.Thread(
            target=self._run, daemon=True)

    def start(self):
        self._thread.start()

    def stop(self):
        self._stop_event.set()
        self._thread.join()

    def ping_registered_devices(self, last_state):
        for chat_id, ip in self._register_manager.alerts:
            connected = Ping().is_ip_connected(ip)
            if ip not in last_state or last_state[ip] != connected:
                msg = f"{ip} is {'not ' if not connected else ''}connected"
                self._bot.send_message(chat_id, msg)
            last_state[ip] = connected

    def _run(self):
        last_state = {}
        while not self._stop_event.is_set():
            self.ping_registered_devices(last_state)
            time.sleep(self.INTERVAL)
