import pytest
import json

from paho.mqtt.client import MQTTMessage
from unittest.mock import MagicMock, patch, mock_open, ANY, call

from telegrambot.mqtt import Mqtt
from telegrambot.telegrambot import TelegramBot
from telegrambot.alerts import Alert
from telegrambot.alerts import AlertsUtils
from telegrambot.alerts import AlertsManager
from telegrambot.alerts import AlertType
from telegrambot.alerts import AlertEncoder


@pytest.fixture()
def alerts_file_contents():
    mock_alert_file_content = [
        {
            "chat_id": 11111,
            "topic": "topic_1",
            "alert_type": "higher",
            "threshold": 100
        },
        {
            "chat_id": 22222,
            "topic": "topic_2",
            "alert_type": "lower",
            "threshold": 10.0
        }
    ]
    file_content_string = json.dumps(mock_alert_file_content, cls=AlertEncoder)
    return file_content_string


class TestAlertsManager:
    @pytest.fixture
    @patch('telegrambot.mqtt')
    def alerts_manager(self, mock_mqtt):
        self.mock_mqtt = mock_mqtt
        return AlertsManager(mock_mqtt)

    def test_alerts_manager_init_load_alerts(self, alerts_manager, alerts_file_contents):
        # Arrange

        # Act
        with patch("builtins.open", mock_open(read_data=alerts_file_contents)) as mock_file:
            alerts_manager.load_alerts()

        # Assert
        self.mock_mqtt.subscribe_async.assert_any_call('topic_1')
        self.mock_mqtt.subscribe_async.assert_any_call('topic_2')

    @patch("builtins.open", new_callable=mock_open)
    def test_register_alert(self, mock_open, alerts_manager):
        alert1 = Alert(chat_id=123, topic='topic',
                       alert_type=AlertType.HIGHER, threshold=100)
        alert2 = Alert(chat_id=123, topic='topic',
                       alert_type=AlertType.HIGHER, threshold=200)
        alerts_manager.register(alert1)
        alerts_manager.register(alert2)

        assert len(alerts_manager.alerts) == 1
        assert alerts_manager.alerts[alert2.chat_id,
                                     alert2.topic, alert2.alert_type].threshold == 200


class TestAlertsUtils:
    @patch("builtins.open", side_effect=[FileNotFoundError, MagicMock()])
    def test_load_alerts_from_file_no_file(self, mock_builtin_open):
        # Arrange

        # Act
        alerts = AlertsUtils.load_alerts_from_file()

        # Assert
        assert len(alerts) == 0

    def test_load_alerts_from_file(self, alerts_file_contents):
        # Arrange

        # Act
        with patch("builtins.open", mock_open(read_data=alerts_file_contents)) as mock_file:
            alerts = AlertsUtils.load_alerts_from_file()

        # Assert
        assert len(alerts) == 2

        assert alerts[11111, 'topic_1', AlertType.HIGHER].chat_id == 11111
        assert alerts[11111, 'topic_1',
                      AlertType.HIGHER].alert_type == AlertType.HIGHER
        assert alerts[11111, 'topic_1', AlertType.HIGHER].threshold == 100
        assert alerts[11111, 'topic_1', AlertType.HIGHER].topic == "topic_1"

        assert alerts[22222, 'topic_2', AlertType.LOWER].chat_id == 22222
        assert alerts[22222, 'topic_2',
                      AlertType.LOWER].alert_type == AlertType.LOWER
        assert alerts[22222, 'topic_2', AlertType.LOWER].threshold == 10.0
        assert alerts[22222, 'topic_2', AlertType.LOWER].topic == "topic_2"

    @patch("builtins.open", new_callable=mock_open)
    def test_write_alerts_to_file(self, mock_open):
        # Arrange
        alerts = [
            Alert(chat_id=11111, topic="topic_1",
                  alert_type="higher", threshold=100),
            Alert(chat_id=22222, topic="topic_2",
                  alert_type="lower", threshold=10.0)
        ]
        expected_json_string = json.dumps(alerts, cls=AlertEncoder)

        # Act
        AlertsUtils.write_alerts_to_file(alerts)

        # Assert
        mock_open.assert_called_once()

        # Verify that 'file.write' was called with the expected JSON string
        file_handle = mock_open()
        file_handle.write.assert_called_once_with(expected_json_string)
