import pytest
from telegrambot.mqtt import MqttUtils


@pytest.mark.parametrize("topic1, topic2, expected_result", [
    ('/banana',     '/banana',          True),  # Test case for same topics
    ('/banana',     '/not_banana',      False), # Test case for different topics
    ('/banana/#',   '/banana/uva',      True),  # Test case for wildcard
    ('/banana/uva', '/banana/#',        True),  # Test case for wildcard
    ('/banana/#',   '/banana/uva/pera', True),  # Test case for wildcard
])

def test_compare_topics(topic1, topic2, expected_result):
    result = MqttUtils.compare_topics(topic1, topic2)
    assert result == expected_result
