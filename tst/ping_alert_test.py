import os
from unittest.mock import MagicMock, patch
import pytest
from telegrambot import config
from telegrambot.alerts import PingAlertHandler, PingAlertRegisterManager
from tst.test_utils import TestUtils


class TestPingAlertRegisterManager:
    @pytest.fixture
    def register_manager(self):
        # def temp ping alert file
        yield PingAlertRegisterManager()
        TestUtils().teardown()


    def test_ping_alert_register_manager_creates_file(self, register_manager):
        assert os.path.isfile(config.BotConfig.PING_ALERTS_FILE)

    def test_ping_alert_register_manager_reads_file(self, register_manager):
        assert register_manager._alerts == set()

    def test_ping_alert_register_manager_register_persists_on_file(self, register_manager: PingAlertRegisterManager):
        # register IP
        ip = '192.168.1.99'
        chat_id = '123'
        register_manager.register(chat_id, ip)
        # create a new instance of register_manager
        new_register_manager_instance = PingAlertRegisterManager(
            config.BotConfig.PING_ALERTS_FILE)
        # check if IP is registered
        assert (chat_id, ip) in new_register_manager_instance._alerts


class TestPingAlertHandler:

    @pytest.fixture
    def mock_bot(self):
        return MagicMock()

    @pytest.fixture
    def register_manager(self):
        yield PingAlertRegisterManager()
        TestUtils().teardown()


    @pytest.fixture
    def handler(self, mock_bot, register_manager):
        return PingAlertHandler(mock_bot, register_manager)

    @patch("telegrambot.utils.Ping.is_ip_connected")
    def test_ping_alert_handler_ping_registered_devices(self, mock_is_ip_connected, mock_bot, handler: PingAlertHandler, register_manager: PingAlertRegisterManager):
        mock_is_ip_connected.return_value = True
        chat_id = '1'
        ip = '192.168.1.123'
        last_state = {ip: False}

        register_manager.register(chat_id,  ip)
        handler.ping_registered_devices(last_state)
        handler.ping_registered_devices(last_state)

        mock_bot.send_message.assert_called_once_with(
            chat_id, f"{ip} is connected")
