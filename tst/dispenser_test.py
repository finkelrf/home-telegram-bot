from unittest.mock import MagicMock, patch

import pytest
from telegrambot.commands import CommandParseError
from telegrambot.dispenser import DispenserCommands, DispenserManager, MqttDispenserService


class TestDispenseManager:
    @pytest.fixture
    def dispenser(self):
        dispenser_service_mock = MagicMock()
        return DispenserManager(dispenser_service_mock)

    def test_dispenser_generate_interval(self, dispenser):
        command = DispenserCommands.START
        duration = 2*60
        num_of_dispenses = 50
        parameters = {
            'duration': duration,
            'repetitions': num_of_dispenses
        }

        dispenser._fill_in_parameters(command, parameters)

        assert len(dispenser._intervals) == num_of_dispenses
        assert sorted(dispenser._intervals) == dispenser._intervals

    @patch('time.sleep')
    def test_dispense_task(self, mock_sleep, dispenser: DispenserManager):
        command = DispenserCommands.START
        duration = 2*60
        num_of_dispenses = 50
        parameters = {
            'duration': duration,
            'repetitions': num_of_dispenses
        }

        dispenser._fill_in_parameters(command, parameters)
        dispenser._is_stopped = False
        dispenser._dispense_task()

        assert dispenser._dispenser_service.send_dispense.call_count == num_of_dispenses

    def test_parse_message(self, dispenser):
        duration_minutes = 120
        expected_duration_seconds = 120*60
        expected_num_of_dispenses = 40
        cmd = f'/treat {duration_minutes} {expected_num_of_dispenses}'

        dispenser.parse_message(cmd)

        assert dispenser._duration == expected_duration_seconds
        assert dispenser._repetitions == expected_num_of_dispenses

    @pytest.mark.parametrize("duration,num_of_dispenses,extra_args",
                             [('not a number', 40, None),
                              (120, 'not a number', None),
                              (120, 40, 40)])
    def test_parse_message_bad_weather(self, dispenser: DispenserManager, duration, num_of_dispenses, extra_args):
        message = f'/treat {duration} {num_of_dispenses} {extra_args}'

        with pytest.raises(CommandParseError) as e_info:
            dispenser.parse_message(message)

    def test_parse_stop_message(self, dispenser: DispenserManager):
        message = f'/treat stop'
        dispenser.parse_message(message)

        assert dispenser._is_stopped


class TestMqttDispenserService:
    def test_send_dispense(self):
        mqtt_mock = MagicMock()
        dispenser_service = MqttDispenserService(mqtt_mock)

        dispenser_service.send_dispense()

        mqtt_mock.publish.assert_called_once()
