import os

from telegrambot import config


class TestUtils:
    def teardown(self):
        self._remove_ping_alerts_file()

    def _remove_ping_alerts_file(self):
        # remove ping alert file
        if os.path.isfile(config.BotConfig.PING_ALERTS_FILE):
            os.remove(config.BotConfig.PING_ALERTS_FILE)
