import pytest

from paho.mqtt.client import MQTTMessage
from unittest.mock import MagicMock, patch, mock_open, ANY

from telegrambot.alerts import Alert
from telegrambot.alerts import AlertType
from telegrambot.telegrambot import TelegramBot
from tst.test_utils import TestUtils


@pytest.fixture()
@patch("builtins.open", new_callable=mock_open)
def telegrambot(mock_open):
    return TelegramBot("", None, MagicMock())


@pytest.mark.skip(reason="Problem with TOKEN validation")
@pytest.mark.parametrize("payload, expected_called", [
    (101, True),  # Test case for alert_low_battery
    (99, False),  # Test case for alert_no_alert
])
@patch("builtins.open", new_callable=mock_open)
@patch("telebot.TeleBot.send_message")
def test_alert_handling(bot_send_message_mock, mock_open, telegrambot: TelegramBot, payload, expected_called):
    # Arrange
    chat_id = 12345
    topic = b'alert_topic'
    threshold = 100

    mqtt_msg = MQTTMessage()
    mqtt_msg.topic = topic
    mqtt_msg.payload = str(payload)

    alert = Alert(chat_id=chat_id, topic=topic,
                  alert_type=AlertType.HIGHER, threshold=threshold)
    telegrambot.alerts_manager.register(alert)

    # Act
    telegrambot.alerts_handler(None, None, mqtt_msg)

    # Assert
    if expected_called:
        bot_send_message_mock.assert_called_once_with(chat_id, ANY)
    else:
        bot_send_message_mock.assert_not_called()


class TestPing:
    @pytest.fixture()
    def telegrambot(self):
        with patch("telebot.TeleBot") as mock_telebot:
            bot = TelegramBot("", None, MagicMock())
            yield bot
            TestUtils().teardown()

    def test_ping_alert(self, telegrambot: TelegramBot):
        telegrambot.run_ping_alert_handler("chat_id",
                                           "/ping_alert register 192.168.1.123")
        telegrambot._bot.send_message.assert_called_with(
            'chat_id', "Ping alerts: 192.168.1.123")

        telegrambot.run_ping_alert_handler("chat_id",
                                           "/ping_alert remove 192.168.1.123")
        telegrambot._bot.send_message.assert_called_with(
            'chat_id', "Ping alerts: ")

        # telegrambot.run_ping_alert_handler("chat_id",
        #                                    "/ping_alert register 192.168.1.1")
        # telegrambot._bot.send_message.assert_called_with(
        #     'chat_id', "Ping alerts: 192.168.1.1")
        # telegrambot.run_ping_alert_handler("chat_id",
        #                                    "/ping_alert register 192.168.1.2")
        # telegrambot._bot.send_message.assert_called_with(
        #     'chat_id', "Ping alerts: 192.168.1.2, 192.168.1.1")
