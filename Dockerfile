FROM python:3.12-alpine

WORKDIR /usr/src/app

COPY . .

RUN pip install .

ENV PYTHONPATH /usr/src/app

CMD [ "telegram-bot" ]
